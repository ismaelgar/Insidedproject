package pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class privateMessagesPage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"main-content\"]/div/div[3]/section/ul/li/div")
    private WebElement listMessages;

    public privateMessagesPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }


    public void verifyPrivateMessages() {

        if (listMessages.isDisplayed()) {
            System.out.println ("User doesn't have private messages");

        } else {

            System.out.println("Here you are the list of private messages");
        }
    }
}
