package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class homePage {

    protected WebDriver driver;
    //private WebDriverWait wait = new WebDriverWait(driver, 6);

    @FindBy (className = "btn_register")
    protected WebElement regButton;

    @FindBy(className = "CTA")
    protected WebElement logButton;

    @FindBy(className = "signOut")
    protected WebElement signOut;

    @FindBy(className = "searchInput")
    protected WebElement searchField;

    @FindBy(className = "searchBTN")
    protected WebElement searchButton;

    @FindBy(xpath = "//*[@id=\"category_50\"]/ul/li[1]/section/h3/a")
    protected WebElement topic;

    @FindBy(className =  "messages")
    protected WebElement privateMessagesSection;

    public homePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

        Assert.assertTrue(verifyBasePageTitle(),"Home Page not loaded");
    }

    //If Register button is visible and enabled, click on it and return Registration page
    public registerPage clickRegBtn() {
        System.out.println("Clicking on register button");
        if(regButton.isDisplayed()||regButton.isEnabled()) {
            regButton.click();
        }
        else {
            System.out.println("Registration process has failed");
        }

        return new registerPage(driver);
    }

    //If LogIn button is visible and enabled, click on it and return Login page
    public loggingPage clickLogBtn() {
        System.out.println("Clicking on LogIn button");
        if(logButton.isDisplayed()||logButton.isEnabled()) {
            logButton.click();
        }
        else System.out.println("Registration process has failed");
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("SignupForm[terms]")));
        return new loggingPage(driver);
    }

    //Enter searching criteria
    public void enterSearch(String searchName){
        searchField.sendKeys(searchName);
    }


    public searchPage clickSearchBtn() {
        System.out.println("Clicking on Search button");
        if(searchButton.isDisplayed()||searchButton.isEnabled()) {
            searchButton.click();
        }
        else System.out.println("Search has failed");
        return new searchPage(driver);
    }

    public topicsPage goToTopics() {
        System.out.println("Going to a Topic");
        if(topic.isDisplayed()||topic.isEnabled()) {
            topic.click();
        }
        else System.out.println("No topic Found!");
        return new topicsPage(driver);
    }

    public privateMessagesPage goToPrivateMessages() {
        System.out.println("Going Private messages");
        if(privateMessagesSection.isDisplayed()||privateMessagesSection.isEnabled()) {
            privateMessagesSection.click();
        }
        else System.out.println("Private messages unavailable");
        return new privateMessagesPage(driver);
    }


    //Captures the title of the page
    public String getPageTitle() {
        String title = driver.getTitle();
        return title;
    }

    //Check the title of the page
    public boolean verifyBasePageTitle() {
        String expectedPageTitle="Join the conversation | inSided Media B.V.";
        return getPageTitle().contains(expectedPageTitle);
    }

    public void logOut() {
        signOut.click();
        driver.close();
    }

}