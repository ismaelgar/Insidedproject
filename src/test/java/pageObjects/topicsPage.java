package pageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class topicsPage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"main-content\"]/div/div[3]/section[2]/ul/li[1]/div[1]")
    private WebElement listTopics;

    public topicsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void verifyTopics() {

        if (listTopics.isDisplayed()) {
            System.out.println ("Here you are the list of topics");

        } else {

            System.out.println("Error - No topics found");
        }
    }
}
