package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class loggingPage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"fancybox-content\"]/div/div/h2/a[2]")
    private WebElement popTitle;

    @FindBy(id = "LoginForm_login")
    private WebElement userNameLocation;

    @FindBy(id = "LoginForm_password")
    private WebElement pwdLocation;

    @FindBy(name = "yt0")
    protected WebElement logBTN;

    @FindBy(id = "LoginForm[remember]")
    private WebElement Remember;

    @FindBy(css = "#LoginForm > div.item.error.clearfix > div")
    private WebElement regErrorMessage;


    public loggingPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    //Method to log a user

    public void logIN(String userName, String password) throws InterruptedException {
        Assert.assertTrue(verifyPopTitle(),"Registration popup not loaded");

        //Enter user and password
        System.out.println("Entering data...");
        userNameLocation.sendKeys(userName);
        pwdLocation.sendKeys(password);

        //Click login button
        logBTN.click();
        System.out.println("Finishing log in...");

        verifyLogin();
    }

    //Method that verifies if the user is logged
    public void verifyLogin() throws InterruptedException {

        Thread.sleep(3000);
        WebDriverWait wait = new WebDriverWait(driver, 6);

        boolean exists = driver.findElements(By.cssSelector("#LoginForm > div.item.error.clearfix > div")).size() == 0;

        if (exists) {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name("yt0")));
            System.out.println("User is succesfully logged");
        } else {
            Assert.fail("Something has not been filled in correctly");
        }
    }

    public String getPopTitle() {
        return popTitle.getText();
    }

    public boolean verifyPopTitle() {
        //Wait until pop is loaded
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("rememberLabel")));
        String expectedPageTitle="Log in";
        return getPopTitle().contains(expectedPageTitle);
    }
}