package pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class searchPage {

    private WebDriver driver;

    @FindBy(className = "searchSummary")
    private WebElement searchSummary;

    public searchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void verifySearch() {


        String results = driver.findElement(By.className("searchSummary")).getText();
        System.out.println(results);


        if (results.equalsIgnoreCase("0 to 0 from 0")) {
            System.out.println ("No results were found");

        } else {

            System.out.println("Results found!!");
        }
    }
}
