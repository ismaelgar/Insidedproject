package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class registerPage {
    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"fancybox-content\"]/div/div/h2/a[1]")
    private WebElement popTitle;

    @FindBy(xpath = "//*[@id=\"SignupForm\"]/div[2]/div[2]/input")
    private WebElement userNameLocation;

    @FindBy(name = "SignupForm[email]")
    private WebElement emailLocation;

    @FindBy(css = "#SignupForm > div:nth-child(7) > div.content > input")
    private WebElement pwdLocation;

    @FindBy(name = "SignupForm[terms]")
    private WebElement terms;

    @FindBy(className = "registerBTN")
    protected WebElement regBTN;

    @FindBy(css = "#SignupForm > div.item.error.clearfix > div")
    private WebElement regErrorMessage;

    public registerPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
//Fill username field
    public void usernameField(String userName){
        userNameLocation.sendKeys(userName);
        System.out.println("Entering data...");

    }

//Fill email field

    public void emailField(String email){
        emailLocation.sendKeys(email);

    }

//Fill password field

    public void passwordField(String password){
        pwdLocation.sendKeys(password);

    }

    public void acceptTerms () {
        if (!terms.isSelected()) {
            terms.click();
        }
        //Assert.assertTrue(terms.isSelected());
        Assert.assertTrue(terms.isSelected(),"Terms not checked");
    }

       public void clickRegister(){
            regBTN.click();
           System.out.println("Finishing registration...");
        }

    //Method that verifies if a user has been successfully registered
    public void verifyRegistration() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 6);
        Thread.sleep(3000);

       boolean exists = driver.findElements(By.cssSelector("#SignupForm > div.item.error.clearfix > div")).size() == 0;

        if (exists) {
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("registerBTN")));
            System.out.println("User succesfully registered");
        } else {
            Assert.fail("Something has not been filled in correctly");
        }
    }

    public String getPopTitle() {
        return popTitle.getText();
    }

    public boolean verifyPopTitle() {
        //Wait until pop is loaded
        WebDriverWait wait = new WebDriverWait(driver, 6);
        wait.until(ExpectedConditions.elementToBeClickable(By.name("SignupForm[terms]")));
        String expectedPageTitle="Register";
        return getPopTitle().contains(expectedPageTitle);
    }

}
