import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.homePage;
import pageObjects.loggingPage;
import pageObjects.registerPage;
import utils.baseSetup;

import static org.testng.Assert.assertTrue;
import static utils.auxFunc.randomWord;


public class authentication extends baseSetup {

    private WebDriver driver;

    @DataProvider
    public Object[][] testDataLogin() {
        return new Object[][] {
                new Object[] { "JohnDoe", "John1234"}
        };
    }

    @BeforeMethod
    public void setUp() {
        driver=getDriver();

    }


    @Test(priority = 1)
    public void registerNewUser() throws Exception {

        homePage homepage = new homePage(driver);
        registerPage registerPage = homepage.clickRegBtn();

        //Check if Registration popup raises
        assertTrue(registerPage.verifyPopTitle(), "Registration popup not loaded");

        //Fulfill credentials
        registerPage.usernameField(randomWord(8));
        registerPage.emailField(randomWord(8) + "@gmail.com");
        registerPage.passwordField("Password1");

        //Accept terms and register
        registerPage.acceptTerms();
        registerPage.clickRegister();
        registerPage.verifyRegistration();

        //Exit application and close browser
        homepage.logOut();


    }

    @Test(priority = 2 ,dataProvider = "testDataLogin")
    public void loginUser(String username, String password) throws Exception {
        homePage homepage = new homePage(driver);
        loggingPage loggingPage = homepage.clickLogBtn();

        //Log into application
        loggingPage.logIN(username,password);

        //Exit application and close browser
        homepage.logOut();


    }

}

