import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import pageObjects.*;
import utils.baseSetup;


public class operations extends baseSetup {

    private WebDriver driver;

    @DataProvider
    public Object[][] dataSearch() {
        return new Object[][]{
                new Object[]{"JohnDoe", "John1234", "Hello"}
        };
    }

    @BeforeMethod
    public void setUp() {
        driver = getDriver();

    }

    @Test(priority = 1, dataProvider = "dataSearch")
    public void searchPosts(String username, String password, String search) throws Exception {
        homePage homepage = new homePage(driver);
        loggingPage loggingPage = homepage.clickLogBtn();

        //Log into application
        loggingPage.logIN(username,password);

        //Fill desired search and submit
        homepage.enterSearch(search);
        searchPage searchPage = homepage.clickSearchBtn();
        searchPage.verifySearch();

        //Exit application and close browser
        homepage.logOut();
    }

    @Test(priority = 2, dataProvider = "dataSearch")
    public void checkTopics(String username, String password, String postBody) throws Exception {
        homePage homepage = new homePage(driver);
        loggingPage loggingPage = homepage.clickLogBtn();

        //Log into application
        loggingPage.logIN(username,password);

        //Check if there are topics
        topicsPage topicsPage = homepage.goToTopics();
        topicsPage.verifyTopics();

        //Exit application and close browser
        homepage.logOut();
    }

    @Test(priority = 3, dataProvider = "dataSearch")
    public void checkPrivateMesssages(String username, String password, String postBody) throws Exception {
        homePage homepage = new homePage(driver);
        loggingPage loggingPage = homepage.clickLogBtn();

        //Log into application
        loggingPage.logIN(username,password);

        //Check if there are topics
        privateMessagesPage privateMessages = homepage.goToPrivateMessages();
        privateMessages.verifyPrivateMessages();


        //Exit application and close browser
        homepage.logOut();
    }
}
